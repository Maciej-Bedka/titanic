import pandas as pd
import numpy as np

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()
    results = []
    titles = [ " Mr. ", " Mrs. ", " Miss. "]

    for title in titles:
        filtered_data = df[df['Name'].str.contains(title, na=False)]

        missing_values_count = df[df['Name'].str.contains(title, na=False)]['Age'].isnull().sum()

        median_age = int(np.nanmedian(filtered_data['Age']))

        df.loc[df['Name'].str.contains(title, na=False) & df['Age'].isnull(), 'Age'] = median_age

        results.append((title.strip(), missing_values_count, median_age))

    return results
